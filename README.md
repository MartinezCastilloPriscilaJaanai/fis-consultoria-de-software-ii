## _NEARSOFT_
##### __Logo__
![Una imagen](imagenes/logo-c1.JPG)
##### __Sitio web__
https://nearsoft.com/
##### __Ubicación__
Hermosillo
Blvd. Antonio Quiroga 21
Col. El Llano
Hermosillo, Sonora, México 83210

https://goo.gl/maps/x1vmxS9xdatgqpex9
##### __Acerca de__
Nearsoft se trata de desarrollo de software. Nuestros equipos se autogestionan y entregan a tiempo. Tratamos nuestro trabajo como un oficio y aprendemos unos de otros.  Constantemente elevamos el listón.
##### __Servicios__
* Desarrollo, pruebas, investigación y ejecución de UX/UI de software.
##### __Presencia__
* https://www.facebook.com/Nearsoft/
* https://twitter.com/Nearsoft
* https://www.linkedin.com/company/nearsoft/
##### __Ofertas laborales__
https://nearsoft.com/join-us/
##### __Blog de ingeniería__
https://nearsoft.com/blog/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Java
* React
* Ruby
* Python
* Node
* DevOps
* Full Stack
* WordPress

## _AVIADA_
##### __Logo__
![Una imagen](imagenes/logo-c2.JPG)
##### __Sitio web__
https://aviada.mx/
##### __Ubicación__
Bulevar Luis D. Colosio 671-12vo Piso, Santa Fe, 83249 Hermosillo, Son.

https://goo.gl/maps/DVUjdTUuPZ9uXsmq8
##### __Acerca de__
Aviada es una empresa de nearshoring y offshoring en México para todas sus necesidades de aumento de personal. Nos especializamos en encontrar y cuidar a grandes empleados en los campos de desarrollo de software, medios digitales, diseño multimedia y otros.
##### __Servicios__
1. Recruitment
2. Legal Compliance
3. Payroll Processing
4. HR
5. Procurement
6. Equipment Leasing
##### __Presencia__
* https://www.facebook.com/aviadamx
* https://www.linkedin.com/company/aviadamx/
* https://www.instagram.com/aviadamx/
* https://twitter.com/AviadaMX

##### __Ofertas laborales__
https://aviada.mx/trabaja-con-aviada/
https://careers.aviada.mx/?hsLang=en
##### __Blog de ingeniería__
https://aviada.mx/category/blog/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Android
* Angular
* AWS/CLOUD
* Data Science
* DevOps
* E-Commerce
* Javascript
* Joomla CMS
* Laravel
* NodeJS
* PHP
* Python
* QA
* React
* React Native
* Swift
* Unity
* Wordpress
* Xamarin
* 3D Animation
* Accessibility (WCAG 2.0)
* Account Management
* Ad Ops / Trafficking
* Ad Bidding 
* AMP
* Campaign Management
* Customer Service
* Graphic Design
* Finance / Accounts Receivable
* Marketing
* Project Management
* Sales Support
* Technical (Web) Support
* UI/UX
* Salesforce Admin
* Video Editing

## _SIMPAT TECH_
##### __Logo__
![Una imagen](imagenes/logo-c3.JPG)
##### __Sitio web__
https://simpat.tech/
##### __Ubicación__
Ave. Eugenio Garza Sada 3820 Torre Micrópolis, Piso 8, Más Palomas (Valle de Santiago), 64860 Monterrey, N.L.
https://goo.gl/maps/nvX2gKpMJNCjJXaa9
##### __Acerca de__
Simpat es una empresa de consultoría de software personalizado con sede en Austin, Texas, con un centro de distribución de software de última generación en Monterrey, Mexico. Somos un equipo de profesionales de software innovadores que buscan alcanzar los desafíos más complejos y las metas de misión crítica de nuestros clientes.
##### __Servicios__
* Software personalizado
* DevOps
* Soluciones Cloud
* Equipos mejorados
* Seguro de calidad
* Inteligencia de negocio
##### __Presencia__
* https://www.facebook.com/simpat.tech
* https://twitter.com/simpat_tech
* https://www.linkedin.com/company/simpattech/
* https://www.instagram.com/simpat.tech/
##### __Ofertas laborales__
https://simpat.tech/careers/
##### __Blog de ingeniería__
https://simpat.tech/insights/
##### __Tecnologías__
Custom Software
* Gold Certified Microsoft Partner for Application Development
* Development methodology, including Agile and Kanban scrum
* Microsoft Stack, including .NET Core, ASP .NET MVC, API, SQL, Entity framework, and C#
* Design patterns, including mediator and dependency Injection
* Mobile development, including ReactNative and Xamarin
* Front-end development, including ReactJS, Angular, and CSS/SASS/CSS Grid
* Containers, including Docker
DevOps
* Silver Certified Microsoft Partner in DevOps
* CI/CD, including Azure pipeline, Kubernetes, Docker, Octopus, Application Insights, and Logging.
Cloud Solutions
* Azure
* AWS
* Kubernetes
* Docker
* PAaS
* RabbitMQ
* Microservices
* Cloud services

## _SVITLA_
##### __Logo__
![Una imagen](imagenes/logo-c4.JPG)
##### __Sitio web__
https://svitla.com/
##### __Ubicación__
C. Colonias #221-piso 4, Col Americana, Americana, 44160 Guadalajara,Jal.

https://goo.gl/maps/sHoHGjSpi6u1Wa8a6
##### __Acerca de__
Somos su conducto hacia las innovaciones tecnológicas más punteras. Ofrecemos un valor incomparable a nuestros clientes, que confían en nuestra experiencia y muchos años de experiencia en Managed Team Extension y AgileSquads.
##### __Servicios__
* Extensión de equipo administrado
* AgileSquads
* Servicio de consultoría
##### __Presencia__
* https://www.facebook.com/SvitlaSystems
* https://www.linkedin.com/company/svitla-systems-inc-/?trk=biz-companies-cym
* https://twitter.com/SvitlaSystemsIn
* https://www.instagram.com/svitlasystems/
* https://www.youtube.com/channel/UC1nu2LV4_08GoZThHEindWA
##### __Ofertas laborales__
https://svitla.com/career
##### __Blog de ingeniería__
https://svitla.com/blog
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Java
* React.js
* Node.js
* CSS3, HTML5, JavaScript y frameworks JavaScript como jQuery, React / Preact, TypeScript, etc.
* Ruby
* Python
* QA
* DevOps

## _TACIT KNOWLEDGE_
##### __Logo__
![Una imagen](imagenes/logo-c5.JPG)
##### Sitio web
https://www.tacitknowledge.com/
##### __Ubicación__
Av. de las Américas 1545-Piso 7, Providencia, 44630 Guadalajara, Jal.

https://goo.gl/maps/YPcvFFjxwvnw7ZF16
##### __Acerca de__
Tacit Knowledge desarrolla, integra y respalda software empresarial para marcas conocidas y minoristas. Ofrecemos soluciones en todos los puntos de contacto del consumidor, desde el dispositivo hasta la puerta. Conectamos todo el ecosistema del negocio de comercio digital de nuestros clientes. Creamos y apoyamos el sitio web de comercio electrónico y las tecnologías asociadas a través del suministro de cumplimiento (Pick & Pack, Kitting ...) hasta las herramientas de entrega, devoluciones y participación del cliente posteriores a la compra.
##### __Servicios__
* Consultoría de Software
* Comercio Digital
* Servicios Gestionados
* Comunidad de código abierto
* Consultoría Ecommerce
* Rendimiento Máximo
* Comercio SAP
* Ncommerce
##### __Presencia__
* https://twitter.com/tacitknowledge
* https://www.linkedin.com/company/tacit-knowledge
##### __Ofertas laborales__
https://www.tacitknowledge.com/careers
##### __Blog de ingeniería__
https://www.tacitknowledge.com/news/
https://www.tacitknowledge.com/thoughts/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Cloud & DevOps
* AWS/GCP/Azure
* pperating relational databases: MySQL, MSSQL, PostgreSQL
* Languages such as Ruby, Python
* QA
* Web technologies: HTTP, Nginx, Apache
* J2EE and JVM based frameworks
* HTML5 and CSS3 web standards

## _TANGO_
##### __Logo__
![Una imagen](imagenes/logo-c6.JPG)
##### __Sitio web__
https://tango.io/
##### __Ubicación__
Av La Paz 51, Colima, Colima 28017, MX

https://goo.gl/maps/AZUBKpcbzvGfYqEc8
##### __Acerca de__
Tango (anteriormente TangoSource) es la principal consultora de desarrollo de productos nearshore y aumento de personal. ¡Creemos en el poder de las ideas innovadoras y la pasión por el producto! Nos enorgullecemos de mantener una cultura diversa y acogedora con un fuerte enfoque en la honestidad, la transparencia y el respeto mutuo. Nos esforzamos por superar no solo las expectativas de nuestros clientes, sino también las expectativas del usuario final. 
##### __Servicios__

##### __Presencia__
* https://www.facebook.com/Tango.ioMX
* https://www.linkedin.com/company/tango-io/
* https://twitter.com/tango_io
* https://www.instagram.com/tangodotio/
##### __Ofertas laborales__
https://jobs.lever.co/tango
##### __Blog de ingeniería__
https://blog.tango.io/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
DevOps
* Jenkins
* Terraform
* Docker
* Heroku
WebDev
* JavaScript
* React
* HTML
* CSS
* Ruby on Rails
* Node
Mobile
* React Native
* Java
* Kotlin
* Swift
* Ionic
* Objetive C
UX/UI
* Adobe XD
* Photoshop
* Illustrator
* Sketch
* Invision
Serveless
* AWS Lambda
* Google Cloud Functions
* Serverless Framework

## _INTELIMETRICA_
##### __Logo__
![Una imagen](imagenes/logo-c7.JPG)
##### __Sitio web__
https://intelimetrica.com/
##### __Ubicación__
Calz. Gral. Mariano Escobedo 748, Anzures, Miguel Hidalgo, 11590 Ciudad de México, CDMX

https://goo.gl/maps/GT94Tg6qcPCKM1iE9
##### __Acerca de__
Intelimetrica tiene la capacidad de transformar nuestros desafíos y problemas comerciales en soluciones analíticas avanzadas. Son flexibles a nuestras necesidades comerciales y adaptan fácilmente sus soluciones para satisfacer los requisitos de nuestros clientes.
##### __Servicios__
* IA
* ML
* Ingeniería de software
* Diseño UX
##### __Presencia__
* https://twitter.com/Intelimetrica
* https://www.facebook.com/intelimetrica
* https://www.linkedin.com/company/intelim%C3%A9trica/about/
* https://github.com/Intelimetrica
##### __Ofertas laborales__
https://intelimetrica.com/careers
##### __Blog de ingeniería__
https://intelimetrica.com/news
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Python, R, Matlab, Java, C, etc
* SQL database systems
* CI/CD, Docker and web scraping
* Frameworks (Flask, FastAPI, Phoenix)
* Software design (SOLID, KISS, DRY, YAGNI)

## _ENROUTE SYSTEMS_
##### __Logo__
![Una imagen](imagenes/logo-c8.JPG)
##### __Sitio web__
http://www.enroutesystems.com/
##### __Ubicación__
Puerta del Sol Nte. 209, Dinastía, 64639 Monterrey, N.L.

https://goo.gl/maps/XCvFmpbYK7bk4T7m7
##### __Acerca de__
Surgimos de una idea visionaria. Artemio, Javier y Alex comenzaron esta empresa con la idea de crear algo genial: un lugar donde las personas puedan trabajar en lo que aman, conocer gente de diferentes orígenes y aprender nuevas formas de involucrarse con la tecnología. Brindamos servicios y soluciones de TI proporcionados por un equipo de personas apasionadas en la resolución de problemas, altamente capacitadas en diversas prácticas comerciales y de TI. Ofrecemos a nuestros clientes cinco servicios principales: control de calidad y pruebas, desarrollo de software, gestión de inventario, ingeniería de datos y mantenimiento de aplicaciones.
##### __Servicios__
* Data platform modernization and migration
* Advanced Analytics and ML
* Business Intelligence & Data Visualization
* Data Migrations and Integrations
* Enterprise Data Management & Digital Transformation
* Software Development
##### __Presencia__
* https://www.linkedin.com/company/enroutesystems/
* https://www.facebook.com/enroutesystems
* https://www.instagram.com/enroutesystems/
##### __Ofertas laborales__
https://enroutesystems.com/careers
##### __Blog de ingeniería__
(no encontrado)
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Java
* React.js
* Node.js
* CSS3, HTML5, JavaScript
* AWS
* Microsoft Azure
* Selenium
* Jenkinks
* Cucumber

## _SCIO CONSULTING_
##### __Logo__
![Una imagen](imagenes/logo-c9.JPG)
##### __Sitio web__
http://www.scio.com.mx
##### __Ubicación__
Av Las Cañadas 501-Int. 230, Tres Marías, 58254 Morelia, Mich.

https://goo.gl/maps/8QoeZZWi8LLMvTrh9
##### __Acerca de__
Somos una empresa de desarrollo de software nearshore y una comunidad de personas apasionadas y orientadas a un propósito. Pensamos de manera disruptiva para entregar tecnología para abordar los desafíos más difíciles de nuestros clientes, todo mientras buscamos revolucionar la industria de TI y crear un cambio social positivo.
##### __Servicios__
* Desarrollo de aplicaciones
 - Aplicaciones Móviles
 - Aplicaciones Web
 - Aplicaciones y Servicios en la Nube
* Desarrollo de aplicaciones de Negocio
* Consultoría Tecnológica
* Soluciones de Software
##### __Presencia__
* https://twitter.com/sciomx
* https://www.facebook.com/ScioMx
##### __Ofertas laborales__
https://www.scio.com.mx/trabaja-scio-mexico/vacantes/
##### __Blog de ingeniería__
https://www.scio.com.mx/blog/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* React Native
* Java
* Kotlin
* Swift
* JavaScript
* React
* HTML
* CSS
* Ruby on Rails
* Node
* Marketing
* Project Management

## _KATA SOFTWARE_
##### __Logo__
![Una imagen](imagenes/logo-c10.JPG)
##### __Sitio web__
https://kata-software.com/es/homeA
##### __Ubicación__
Vito Alessio Robles 300, Florida, Álvaro Obregón, 01030 Ciudad de México, CDMX

https://goo.gl/maps/xoBmQcFWmRjF7dUm7
##### __Acerca de__
"Kata Software nos ayuda a estar cerca de nuestros clientes sin importar dónde se encuentren. La tecnología móvil es sin duda un habilitador que nos ha permitido alcanzar nuestro objetivo: llevar servicios financieros a los sectores populares."
##### __Servicios__
* IA para generar análisis de datos
* Seguridad
* Soluciones SaaS
* Operación de productos financieros para sistemas de cobranza
* software contables
* sistemas de originación
* software de gestión financiera 
##### __Presencia__
* https://www.facebook.com/Kata-Software-109450663980021/
* https://www.linkedin.com/company/37521547/
##### __Ofertas laborales__
https://kata-software.com/es/trabaja-con-nosotros
##### __Blog de ingeniería__
https://kata-software.com/es/publicaciones
##### __Tecnologías__
Cloud Solutions
* Azure
* AWS
Custom Software
* Gold Certified Microsoft Partner for Application Development
Frameworks
* Kata Builder (framework propio)

## _NT GROUP_
##### __Logo__
![Una imagen](imagenes/logo-c11.JPG))
##### __Sitio web__
https://www.next-technologies.com.mx
##### __Ubicación__
Col del Valle Centro. Benito Juárez. 03100 Ciudad de México, CDMX

https://goo.gl/maps/nRNNAeGgcW51umCBA
##### __Acerca de__
En NT Group ofrecemos soluciones de alta tecnología en TI en todas las actividades de desarrollo económico, especialistas en Hospitality. Somos un motor de vanguardia que ayuda al desarrollo de pequeñas y medianas e
##### __Servicios__
Ofrece los siguientes Productos y Servicios
* FACTO Hospitality
* FACTO Business
* FACTO Orders
* FACTO Valid
* FACTO Conta
* FACTO Payroll
* Analytics
* RFC Check
##### __Presencia__
* https://www.facebook.com/NextTechInt/
* https://twitter.com/nexttechmx
* https://www.linkedin.com/company/nexttechnologiesworld
##### __Ofertas laborales__
https://www.getonbrd.com/empleos/data-science-analytics/data-engineer-jr-nt-group-remote
##### __Blog de ingeniería__
(no encontrado)
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Microsoft Azure
* AWS
* Google Cloud Plataform
* etc

## _SERTI_
##### __Logo__
![Una imagen](imagenes/logo-c12.JPG))
##### Sitio web
https://serti.com.mx/
##### __Ubicación__
Shakespeare 6-piso 3, Verónica Anzúres, Miguel Hidalgo, 11590 Ciudad de México, CDMX

https://goo.gl/maps/X1RFKqeVZM5LyfWD8
##### __Acerca de__
Ofrecemos desarrollo de software basado en productos o servicios Web, utilizando ciclos de desarrollo “agiles” que incluyan un fuerte análisis, diseño, desarrollo, pruebas y en caso de necesitarlo mantenimiento, nos gusta desarrollar software web con un alto contenido de User-Experience, pantallas fáciles de utilizar esto ayudara no solo a tus usuarios si no al modelo de tu negocio.
##### __Servicios__
* Desarrollo de plataformas web
* Desarrollo gráfico
* Desarrollo de apps
* Reclutamiento IT
##### __Presencia__
* https://twitter.com/serTI_Mx
* https://www.facebook.com/SERTIMX/
* https://www.linkedin.com/company/servicios-estrat-gicos-en-tecnolog-as-de-informaci-n/
##### __Ofertas laborales__
https://serti.com.mx/unete/
##### __Blog de ingeniería__
https://serti.com.mx/serti/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Java
* Spring Web
* .NET
* C++
* GlassFish
* Spring Batch
* Bootstrap
* Spring Security
* MySQL
* HIGHCHARTS
* HIBERNATE
* HTML5
* CSS
* JS
* WordPress
* WooCommerce
* PS

## _ONEPHASE_
##### __Logo__
![Una imagen](imagenes/logo-c13.JPG))
##### Sitio web
https://onephase.com
##### __Ubicación__
Av. Revolución 4020-Local 6, Colonial La Silla, 64860 Monterrey, N.L.

https://goo.gl/maps/ej2wycaBh6a9cnd66
##### __Acerca de__
Somos una empresa de consultoría tecnológica, ofrecemos soluciones tecnológicas personalizadas basadas en un enfoque centrado en el cliente. En Onephase ayudamos a su empresa a navegar en su viaje hacia la Transformación Digital.
##### __Servicios__
* Digital Consulting
* Software Development
* Software Quality Assurance & Testing
* E-commerce Solutions
* Nearshore Staffing
* UI/UX Design
* VR/AR Services
* Cloud Services
* Big Data
##### __Presencia__
* https://twitter.com/OnePhaseLLC
* https://www.instagram.com/onephasellc/
* https://www.facebook.com/OnephaseLLC/
##### __Ofertas laborales__
(no encontrado)
##### __Blog de ingeniería__
https://onephase.com/insight
##### __Tecnologías__
Front-end
* Vue
* React
* Angular
* Unity
* ASP
Back-end
* Rails
* Laravel
* Django
* NodeJS
* .NET
Mobile
* Ionic
* Xamarin
* React
* Android
* Swift
* Unity
Data Base
* MySQL
* MongoDB
* Redis
* PostgreSQL
* SQL
Cloud Services
* Jenkinks
* Azure
* AWS
* Docker
* Heroku

## _AGILE THOUGHT_
##### __Logo__
![Una imagen](imagenes/logo-c14.JPG))
##### __Sitio web__
https://agilethought.com/
##### __Ubicación__
Bosques de Alisos 45B, Bosques de las Lomas, Cuajimalpa de Morelos, 05120 Ciudad de México, CDMX

https://goo.gl/maps/MztxaorBwmwgkDx56
##### __Acerca de__
AgileThought es un socio de software personalizado y de transformación digital de servicio completo en el que Fortune 1000 confía para el diseño, el desarrollo, el soporte y la orientación para acelerar la innovación y garantizar resultados excepcionales durante todo el ciclo de vida del producto.
##### __Servicios__
Consultoría Estratégica
* Gestión de productos
* Transformaciones organizacionales
* Capacitación y certificaciones
Estrategia Digital
* Ingeniería de aplicaciones
* Arquitectura y migración de la nube
* Análisis de datos avanzado
* Automatización
* Modernización de aplicaciones
* Diseño UX/IU
* IA y ML
##### __Presencia__
* https://www.linkedin.com/company/agilethought/
* https://www.facebook.com/AgileThought
* https://twitter.com/AgileThought
* https://www.instagram.com/agilethought/
##### __Ofertas laborales__
https://agilethought.com/careers/
##### __Blog de ingeniería__
https://agilethought.com/insights/?_sft_category=blogs#resources
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.

## _GFT_
##### __Logo__
![Una imagen](imagenes/logo-c15.JPG))
##### __Sitio web__
http://www.gft.com/mx
##### __Ubicación__
Gobernador Agustín Vicente Eguia 46, San Miguel Chapultepec I Secc, Miguel Hidalgo, 11850 Ciudad de México, CDMX

https://goo.gl/maps/YYRD2kNeRYUJi4G4A
##### __Acerca de__
GFT es una empresa multinacional de consultoría y proyectos de tecnología de la información. Somos una de las empresas más reconocidas del mundo en proyectos de integración de sistemas para canales digitales, contamos con amplia experiencia en la habilitación de cambios de negocios para resolver los desafíos más críticos de nuestros clientes. Fundada en 1987, GFT contamos con más de 6,000 profesionales en 15 países para poder garantizar la proximidad con nuestros clientes. GFT México opera desde 2015, donde contamos con un equipo de cerca de 400 profesionales.
##### __Servicios__
* IA
* Cloud
* Análisis de datos
* GreenCoding
##### __Presencia__
* https://www.linkedin.com/company/gft-group/
* https://www.facebook.com/gft.mex
* https://twitter.com/gft_mx
* https://www.youtube.com/user/gftgroup
* https://www.instagram.com/gft_tech/
##### __Ofertas laborales__
https://www.gft.com/mx/es/index/compania/empleo/
##### __Blog de ingeniería__
https://blog.gft.com/es/
##### __Tecnologías__
No sólo trabajamos en la digitalización de procesos, sino que también somos especialistas en la gestión y explotación de los datos y en la implementación de nuevas tecnologías (IoT, IA, Blockchain y DLT, Cloud y análisis de datos), algo que estamos aplicando, especialmente, para nuestros clientes del sector Industria (automoción, manufacturero…)

## _NOVA_
##### __Logo__
![Una imagen](imagenes/logo-c16.JPG))
##### __Sitio web__
http://novasolutionsystems.com/
##### Ubicación
Eje 4 Sur 535-Piso 29, código 2, Del Valle Norte, Benito Juárez, 03103 Ciudad de México, CDMX

https://goo.gl/maps/wuDwfG8fA8BQHhNm6
##### __Acerca de__
En Nova nuestra principal experiencia  se basa en fusionar el conocimiento de trabajar con grandes instituciones y la agilidadque se obtiene en fintechs o empresas nativas digitales. “Aceleramos la adopción digital en el sector financiero en México”.
##### __Servicios__
* Recursos Digitales
* Seguridad Financiera
* Transformación Digital
* Estrategia Digital
* Fintech Factory
##### Presencia
* https://www.facebook.com/novasolutionsystems/
* https://twitter.com/novasolutionsys/
* https://www.linkedin.com/company/novasolutionsystems/
##### Ofertas laborales
http://novasolutionsystems.com/unete/#vacantes
##### Blog de ingeniería
http://novasolutionsystems.com/conocenos/
##### Tecnologías
* Oracle
* Jenkins
* Java
* Git
* V
* Angular
* MongoDB
* SQL Server
* PostgreSQL
* Python
* Mojito
* Node.js
* Arduino
* C++

## _PROSCAI_
##### Logo
![Una imagen](imagenes/logo-c17.JPG))
##### Sitio web
 https://www.proscai.com/
##### Ubicación
Av. Pdte. Masaryk 101-601, Polanco, Polanco V Secc, Miguel Hidalgo, 11560 Ciudad de México, CDMX

https://goo.gl/maps/oYJ1YUnT87rvCnK59
##### Acerca de
Empresa líder en el sector tecnológico dedicados al desarrollo e implementación de software ERP para más de 300 empresas en México, la cual está en constante búsqueda de jóvenes talentos.
##### Servicios
* Desarrollo
* Implementación
* Capacitación
* Consultoría
* Soporte
##### Presencia
* https://twitter.com/proscai
* https://www.linkedin.com/company/proscai/
##### Ofertas laborales
(no encontrado)
##### Blog de ingeniería
https://blog.proscai.com/
##### Tecnologías
Tecnologías, lenguajes de programación, frameworks, etc.
* AWS
* Microsoft Azure
* Java
* React.js
* Node.js
* CSS3, HTML5, JavaScript
* Jenkinks
* Cucumber

## _MOBIIK_
##### Logo
![Una imagen](imagenes/logo-c18.JPG))
##### Sitio web
https://www.mobiik.com/
##### Ubicación
Vito Alessio Robles 166, Florida, Benito Juárez, 01030 Ciudad de México, CDMX

https://goo.gl/maps/pcf9ddxquVo9qpNQ8
##### Acerca de
Mobiik reúne a expertos altamente reconocidos en la industria por sus destrezas en el desarrollo de aplicaciones estratégicas de alto impacto. Haciendo uso de la metodología de experiencia de usuario y metodología de desarrollo ágil SCRUM se asegura el éxito del proyecto incrementando el potencial de las empresas al mejorar la experiencia del usuario y reducir los costos en servicios de consultoría e implementación.
##### Servicios
* Desarrollo de apliacaciones
* Servicios en la nube
* Data Science
* IA
##### Presencia
* https://www.facebook.com/MobiikES/
* https://www.linkedin.com/company/mobiik/
##### Ofertas laborales
No hay empleos ahora mismo.
##### Blog de ingeniería
(no tiene)
##### Tecnologías
Tecnologías, lenguajes de programación, frameworks, etc.
* Angular
* CSS
* HTML5
* SQL Server
* .NET
* .NETCARE
* kubernetes
* Docker
* Azure

## _GROW IT_
##### __Logo__
![Una imagen](imagenes/logo-c19.JPG))
##### __Sitio web__
https://www.grw.com.mx/
##### __Ubicación__
San Antonio 120, Nápoles, Benito Juárez, 03840 Ciudad de México, CDMX

https://goo.gl/maps/kpyoiZaXtbsj8Yuj8
##### __Acerca de__
Grow IT es una empresa de desarrollo de software especializada en productos de Microsoft Dynamics. Ayudamos a los partners a reducir la brecha entre las capacidades del producto y las necesidades de sus clientes utilizando metodologías de desarrollo de software para planear y controlar el ciclo de desarrollo, lo que asegura la calidad del producto final optimizando la inversión.
##### __Servicios__
* Desarrollo de Software
##### __Presencia__
* https://www.linkedin.com/company/grow-it-consulting/about/
* https://www.facebook.com/growitc
##### __Ofertas laborales__
https://www.grw.com.mx/bolsa-de-trabajo/
##### __Blog de ingeniería__
https://www.grw.com.mx/blog/
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Dynamics AX
* Dynamics AX for Retail
* Dynamics CRM 
* Dynamics SL
* Azure

## _ICALIA LABS_
##### __Logo__
![Una imagen](imagenes/logo-c20.JPG))
##### __Sitio web__
https://www.icalialabs.com/
##### __Ubicación__
Av. Eugenio Garza Sada 3820 Más Palomas (Valle de Santiago) 64860 Monterrey, Nuevo León

https://goo.gl/maps/29E76q6C68W1EpR5A
##### __Acerca de__
Icalia es una empresa de desarrollo de software personalizado con años de experiencia probada. Combinamos una profunda experiencia en la industria y lo último en TI para brindar soluciones que prosperen en la era de la transformación digital. Icalia Labs está listo para abordar proyectos de cualquier industria. 
##### __Servicios__
* Ruby on Rails (RoR)
* Stuff Augmentation Services
* Digital Transformation
* Design Sprint & Workshops
* Custom Software Development Services
##### __Presencia__
* https://www.linkedin.com/company/icalia-labs/
* https://clutch.co/profile/icalia-labs#summary
* https://github.com/IcaliaLabs
* https://dribbble.com/icalialabs/members
##### __Ofertas laborales__
No hay empleos ahora mismo.
##### __Blog de ingeniería__
https://www.icalialabs.com/blog-software-development-tips-and-news
##### __Tecnologías__
Tecnologías, lenguajes de programación, frameworks, etc.
* Angular
* AWS/CLOUD
* DevOps
* E-Commerce
* Javascript
* NodeJS
* PHP
* Python
* QA
* React
* React Native
* Wordpress
* Account Management
* AMP
* Campaign Management
* Customer Service
* Graphic Design
* Finance / Accounts Receivable
* Project Management
* UI/UX

## FINAL
Este es un reporte sobre 20 consultorías que tienen presencia en México.